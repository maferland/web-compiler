# Web-Compiler

How to start : 

1. sudo apt-get install python-pip
2. sudo pip install virtualenv
3. virtualenv [name your env]
4. cd {env}/bin
5. . activate
6. sudo pip-install flask
7. sudo apt-get install git
8. git clone https://bitbucket.org/maferland/web-compiler
10. cd app
11. sudo python app.py

Et voilà !

(serve on 127.0.0.1:5000)