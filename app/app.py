from flask import Flask, render_template, request
from subprocess import Popen, PIPE
import sys


app = Flask(__name__)

@app.route('/', methods=['GET'])
def indexGet():
    return render_template('index.html', code="")
        
@app.route('/', methods=['POST'])
def indexPost():
    code = request.form["text"]
    
    with open('code.cpp', 'w') as file_:
        file_.write(code)
    file_.close()
    
    process = Popen(['g++', 'code.cpp', '-o code'], stdout=PIPE, stderr=PIPE)
    
    out, err = process.communicate()
    errcode = process.returncode
    return render_template('index.html', errcode=errcode, err=err)
     
if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf8')
    app.run(debug=True, host='0.0.0.0')

